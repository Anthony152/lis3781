> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Anthony Threlfall

### LIS3781 Requirements:

*Course Work Links:*

*Assignment:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Provide screenshots of A1 ERD
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - A2 SQL code
        - Create tables
        - Populate tables
    - Ch.11 Questions
    - Create users and give them privilages locally.  
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Connect to Oracle server
    - A3 SQL code
        - Create tables
        - Populate tables
    - Ch.12 Questions
    - SQL code for required report
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - A4 SQL code
        - Create tables
        - Populate tables
    - Ch.14 Questions
    - SQL code for required report
    - Entity Relationship Diagram
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - A5 SQL code
        - Create tables
        - Populate tables
    - Ch.15 Questions
    - SQL code for required report
    - Entity Relationship Diagram

*Projects*

1. [Project 1](project1/README.md "My project 1 README.md file")
    - Project 1 SQL code
        - Create tables
        - Populate tables
    - Ch.13 Questions
    - Entity Relationship Diagram
    - SQL code for required reports

2. [Project 2](project2/README.md "My project 2 README.md file")
    - Install MongoDB
    - Install Atlas
    - Ch. 16 Questions
    - MongoDB commands
