> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 3 Requirements:

*Five Parts:*

1. Connect to Oracle server
2. Create and populate tables
3. Chapter Questions (Chs 12)
4. SQL code fo required report
5. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables
* Optional: SQL code for the required reports. 


#### Assignment Screenshot:

#### Screenshot of SQL code:

![SQL code](img/code.png "SQL code") 

#### Screenshot of populated tables:

![Populated tables](img/tables.png "Populated tables")  

#### SQL code for the required reports:

![SQL code for report](img/report.png "SQL code for report") 
