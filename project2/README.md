> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management
## Anthony Threlfall

### Project 2 Requirements:

*Five Parts:*

1. Install MongoDB
2. Install Atlas
3. Ch 16 Questions
4. MongoDB commands
5. bitbucket repo links:

   



#### README.md file should include the following items:

* Screenshot of MongoDB commands
* Screenshot of code for required reports

 

#### Assignment Screenshots:

#### Screenshot of MongoDB commands:

![MongoDb commands](img/mcom.png "mdb code")



#### Screenshot of Assignement 5 required report 1:
![p2 Ex Screenshot](img/answ.png "pd Ex Screenshot")

