> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management
## Anthony Threlfall

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installations
3. Questions
4. Entity Relationship Diagram
5. bitbucket repo links:

    a) this assignment and

    b) the completed tutorial (bitbucketstationlocation)

## A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents
of both the employee and any respective dependents. In addition employees' histories must be tracked. Also include the following business rules:

* Each employee may have one or more dependents.
* Each employee has only one job. 
* Each job can be held by many employees.
* Many employees may receive many benefits.
* Many benefits may be selected by many employees (though, while they may not select any benefits - any dependents of employees may be on an employee's plan).
*Employee/Dependent tables must use suitable attributes (See Assignment Guidelines).

In Addition:

* Employee:SSN,DOB,start/end dates, salary.
* Dependent same information as their associated employee (though not start/end dates), date added (as dependent), type of relationship: e.g. father, mother, etc.
* Job: title (e.g. secretary, service tech, manager, cashier, janitor, IT, etc.)
* Benefit: name (e.g. medical, dental, long-term disability, 401k, life insurance, etc.)
* Plan: type (single, spouse, family) cost, election date (plans must be unique)
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why.
* Zero Filled data: SSN, zip codes (not phone numbers, US area codes not below 201, NJ).
* All tables must include notes attribute.

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* Git commands w/short descriptions



#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one.
2. git status - Show the working tree status.
3. git add - Add file contents to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Fetch from and integrate with another repository or a local branch.
7. git rm - Remove files from the working tree and from the index.
 

#### Assignment Screenshots:

#### Screenshot of AMPPS:

![AMPPS screenshot](img/ampps.png "AMPPS screenshot")

#### A1 ERD Screenshot:

![A1 ERD Screenshot](img/erd.png "A1 ERD Screenshot")

#### Screenshot of A1 Ex.1:
![A1 Ex.1 Screenshot](img/a1_ex1.png "A1 Ex.1 Screenshot")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 bitbucket station Locations Tutorial Link](https://bitbucket.org/Anthony152/bitbucketstationlocations/ "Bitbucket Station Locations")
