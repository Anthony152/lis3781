> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management
## Anthony Threlfall

### Assignment 5 Requirements:

*Five Parts:*

1. MS SQL code
2. MS SQL code for required reports
3. Ch 15 Questions
4. Entity Relationship Diagram
5. bitbucket repo links:

   
## Assignment 5 Database Business Rules:

Expanding upon the high-volumehome office supply company’s data tracking requirements, the CFO  
  requests your services again to extend the data model’s functionality. The CFO has read about the   
  capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a  
  smaller data mart as a test platform. He is under pressure from the members of the company’s board of 
  directors who want to reviewmore detailed salesreports based upon the following measurements:

1. Product
2. Customer
3. Sales representative
4. Time(year, quarter, month, week, day, time)
5. Location

  Furthermore, the board members want location to be expanded to include the following characteristics of 
  location:

1. Region
2. State
3. City
4. Store



#### README.md file should include the following items:

* Screenshot of MS SQL code
* Screenshot of Assignment 5 ERD
* SQL code for requred report

 

#### Assignment Screenshots:

#### Screenshot of SQL code:

![SQL code](img/sqlcode.png "sql code")

#### Screenshot of Assignment 5 table:
![table](img/table.png "Assignment table Screenshot")

#### Assignment 5 ERD Screenshot:

![A5 ERD Screenshot](img/erd.png "A5 ERD Screenshot")

#### Screenshot of Assignement 5 required report 1:
![A5 Ex Screenshot](img/answ.png "A5 Ex Screenshot")

