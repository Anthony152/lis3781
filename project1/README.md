> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management
## Anthony Threlfall

### Project 1 Requirements:

*Five Parts:*

1. SQL code
2. SQL code for required reports
3. Ch 13 Questions
4. Entity Relationship Diagram
5. bitbucket repo links:

   
## Project 1 Database Business Rules:

As the lead DBA for a local municipality, you are contacted by the city council to design a database in  
  order to track and document the city’scourt case data. Some report examples:  
  Which attorney is assigned to what case(s)?  
  How many unique clients have cases(be sureto add a client tomore than one case)?  
  How many cases has each attorney been assigned, and names of their clients (return number and names)?  
  How many cases does each client have with the firm(return a name and number value)?  
  Which types ofcases does/dideach client have/hadand their start and end dates?  
  Which attorney is associated to which client(s), and to which case(s)?  
  Names of three judges with the most number of years in practice, include number of years.  
  Also, include the following business rules:  

* An attorney is retainedby (or assigned to)one or moreclients,for each case.
* A client has(or is assigned to) one or more attorneysfor each case.
* An attorney hasone or more cases.
* A client has one or more cases.
* Each court has one or more judges adjudicating.
* Each judge adjudicates upon exactly one court.
* Each judgemaypresideovermore than one case.
* Each case that goes to court ispresided over by exactly one judge.
* A person can have more than one phone number.

  Notes:  

* Attorneydata must include social security number, name, address, office phone,  
  home phone,e-mail,start/end dates, dob, hourly rate, years in practice,  
  bar(may be more than one-multivalued), specialty(may be more than one-multivalued).
* Clientdata must include social security number, name, address, phone, e-mail, dob.
* Case datamust include type, description, start/end dates.
* Court data must include name, address, phone, e-mail, url.
* Judge data must include sameinformation as attorneys(except bar, specialtyand hourly rate;instead, use salary).
* Must track judge historicaldata—tenure at each court (i.e., start/end dates), and salaries.
* Also, history will track which courts judgespresided over,if they took a leave of absence, or retired.
* Alltables must have notes.

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of Project 1 ERD
* SQL code for requred report

 

#### Assignment Screenshots:

#### Screenshot of SQL code:

![SQL code](img/sqlcode.png "sql code")

#### Screenshot of Project 1 table:
![table](img/table.png "Project 1 table Screenshot")

#### Project 1 ERD Screenshot:

![P1 ERD Screenshot](img/erd.png "P1 ERD Screenshot")

#### Screenshot of Project 1 required report 1:
![P1 Ex.1 Screenshot](img/answ.png "P1 Ex.1 Screenshot")

