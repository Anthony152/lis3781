> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management
## Anthony Threlfall

### Assignment 2 Requirements:

*Four Parts:*

1. A2 SQL code
2. Questions
3. Create users and give privilages locally
4. bitbucket repo link

## Assignment 2

1. Use SQL only.
2. Locally: create at18d database, and two tables: company and customer.
    
    Note: Also, these tables must be populated in at18d databse on the CCI server.

        a) Use 1:M relationship: company is parent table
        b) company attributes:
            i. cmp_id(pk)
            ii. cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership')
            iii. cmp_street
            iv. cmp_city
            v. cmp_zip(zf)
            vi. cmp_phone
            vii cmp_ytd_sales
            ix. cmp_url
            x. cmp_notes
        c) customer attributes:
            i. cus_id(pk)
            ii. cmp_id(fk)
            iii. cus_ssn(binary 64)
            iv. cus_salt(binary 64)
            v. cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering')
            vi. cus_first
            vii. cus_last
            viii. cus_street
            ix. cus_city
            x. cus_state
            xi. cus_zip(zf)
            xii. cus_phone
            xiii. cus_email
            xiv. cus_balance
            xv. cus_tot_sales
            xvi. cus_notes
        d) Create suitable indexes and foreign keys:
        Enforce pk/fk relationship: on update cascade, on delete restrict

 
#### README.md file should include the following items:

* Screenshot of A2 SQL code;
* Screenshot of populated tables;


#### Assignment Screenshots:

#### A2 ERD SQL code:

| ![A2 SQL code](img/a2.1.png "A2 SQL code") | ![A2 SQL code](img/a2.2.png "A2 SQL code") |
| ------------------------------------------ | -------------------------------------- |
| ![A2 SQL code](img/a2.3.png "A2 SQL code") | ![A2 SQL code](img/a2.4.png "A2 SQL code") |

#### Screenshot of A2 populated tables:
![A2  populated tables](img/result.png "A2 populated tables")

