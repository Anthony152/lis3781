def get_requirements():
    print("\nDeveloped by Anthony Threlfall")
    print("Python Lists")
    print("\nProgram Requirements:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
        + "3. Create list - using square brackets [list]: my_list = ["cherries", "apples", "bananas", "oranges"]\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format."
        + "Note: user enteres number of requested list elements, dynamically rendered below (that is, number of elemts can change each run).")

def get_input():
     